import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>ProjectCloud Labs Information Tech Company</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <strong>PCL!</strong>
        </h1>

        <p className={styles.description}>
          Our service{' '}
          {/* <code className={styles.code}>pages/index.js</code> */}
        </p>

        <div className={styles.grid}>
          <a href="#" className={styles.card}>
            <h3>Custom Tech Solutions &rarr;</h3>
            <p>We digest your environment and offer best solutions for your budget.</p>
          </a>

          <a href="#" className={styles.card}>
            <h3>On-site development &rarr;</h3>
            <p>You may hire us as a team or as a team player</p>
          </a>

          <a
            href="#"
            className={styles.card}
          >
            <h3>Start-up &rarr;</h3>
            <p>We love startups. If you have an Idea let's talk. <code className={styles.code}>info @ pcl.com.tr</code></p>
          </a>

          <a
            href="#"
            className={styles.card}
          >
            <h3>Training &rarr;</h3>
            <p>
              .Net Core, React trainings waiting for you.
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
